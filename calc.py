

def sumar(num1, num2):#Funcion para hacer la suma
    suma = num1 + num2
    return suma

def restar(num1,num2):#Funcion para calcular la diferencia
    diferencia = num1 - num2
    return diferencia

if __name__ == "__main__": #Programa principal
    print(f"La suma de 1 y 2 es {sumar(1,2)}")
    print(f"La suma de 3 y 4 es {sumar(3, 4)}")
    print(f"La resta de 6 y 5 es {restar(6,5)}")
    print(f"La resta de 8 y 7 es {restar(8, 6)}")